"""Custom configuration of jupyter network server so that
the notebooks are exposed in addresses other than localhost.

Following

https://stackoverflow.com/questions/42848130/why-i-cant-access-remote-jupyter-notebook-server

"""

c.NotebookApp.allow_origin = '*' #allow all origins
c.NotebookApp.ip = '0.0.0.0' # listen on all IPs
