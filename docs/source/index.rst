.. dx-punch documentation master file, created by
   sphinx-quickstart on Tue Dec 18 10:46:56 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the documentation  ``dx_punch``
==========================================

A package for punching-shear design of reinforced-concrete slabs
----------------------------------------------------------------

.. figure:: /_static/sample_ui_check.png
   :alt: Sample ui checks
   :width: 80%
   :align: center

   A sample plot of design check on the most adverse control perimeter
   :math:`u_i`.

Features
^^^^^^^^

Design according to Eurocode provisions (EN-1992-1)
"""""""""""""""""""""""""""""""""""""""""""""""""""

1. Evaluation of basic control perimeters (§6.4.2)

   * Support for columns of arbitrary polygonal shape.
   * Support for drop panels.
   * Automatic classification of columns into 'internal', 'edge',
     and 'corner'.

2. Evaluation of design shear stress (§6.4.3) for biaxial eccentricity
   of all classes of columns, using three methods:

   * Analytical via numerical evaluation of the perimeter modulus :math:`W_1`
     (6.40) in both directions.
   * Approximate through Eq. (6.43).
   * Simplified through §6.4.3(6).

3. Design checks according to §6.4.4.

   * Support for declaration of soil-pressure on raft-slabs:

     * Uniform
     * Discrete pressure field

4. Evaluation of a code-compliant shear-cage layout according to
   provisions of §6.4.5.

*Not yet supported*:

* Consideration of openings in the evaluation of basic control perimeters
  (§6.4.2(3)).
* Favourable effect of in-plane normal stresses :math:`\sigma_{\text{cp}}`
  in Eq. (6.47).
* Evaluation of radial layout of shear reinforcement (Fig. 6.22A).

API
"""

We use the ``dx`` stack of packages (`dx-utilities`_, `dx-base`_, `dx-eurocode`_)
that enables:

#. Geometric representation of structural elements using `shapely`_.
#. Geometric operations and linear algebra utilities with `numpy` and `mathutils`.
#. Use of `dx_utilities.fields` for various representations.
#. Visualization of results using `matplotlib`_.
#. Support for tabular reports using `pandas`.

Usage
-----
.. toctree::
   chapters/usage

.. toctree::
   :hidden:
   :glob:

   _modules/modules

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
