********
API docs
********

**NOTE**: Any references to sections, equations, figures etc. in the
documentation of :py:mod:`dx_punch.EC2` map to the provisions of `Eurocode 2`_.

.. include:: dx_punch.rst
