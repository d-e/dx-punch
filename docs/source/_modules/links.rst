.. Add links to use in source pages in the current folder

.. |create_as_column_offset| replace:: `create_as_column_offset
   <dx_base.elements.ColumnDropPanel.create_as_column_offset>`

.. |RCSlab.add_partial_rebar| replace:: `RCSlab.add_partial_rebar
   <dx_base.elements.RCSlab.add_partial_rebar>`

.. |RCSlab.add_rebar_raw| replace:: `RCSlab.add_rebar_raw
   <dx_base.elements.RCSlab.add_rebar_raw>`

.. |RCSlab.add_tension_rebar_raw| replace:: `RCSlab.add_tension_rebar_raw
   <dx_base.elements.RCSlab.add_partial_rebar>`

.. |PlanarShape.new| replace:: `PlanarShape.new
   <dx_utilities.geometry.planar.PlanarShape.new>`

.. _Eurocode 2: https://eurocodes.jrc.ec.europa.eu/showpage.php?id=132
