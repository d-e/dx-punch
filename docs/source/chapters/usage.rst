Using `dx-punch`
================

Workflow
--------

1. Define geometry and material data of the slab:
   `Slab.new <dx_utilities.geometry.planar.PlanarShape.new>`

2. Define reinforcement:

   * Uniform: `Slab.add_uniform_rebar
     <dx_base.elements.RCSlab.add_uniform_rebar>`
   * Partial: `Slab.add_partial_rebar`

3. Define soil-pressure [raft slabs]:

   * Uniform:
     `Slab.add_soil_pressure
     <dx_base.elements.RCSlab.add_soil_pressure>`
   * Discrete:
     `Slab.add_discrete_soil_pressure
     <dx_base.elements.RCSlab.add_discrete_soil_pressure>`

3. Define columns on slab:
   `Slab.add_column`

4. Define any drop-panels on the columns:
   `Column.set_drop_panel`

5. Inspect results:

   *Design checks are executed implicitly when
   any type of results is sought.*

   * `Slab.passed_columns`
   * `Slab.failed_columns`
   * `Column.output`

   Postprocessor utilities:

   * `Slab.postprocessor`

     * `SlabPostProcessor.plot_u0`:
       Plots the demand-to-capacity ratio, :math:`{\mathsf v_{ed}}/{
       \mathsf v_{rd}}`, at the sides of the columns (§6.4.5(3)).
     * `SlabPostProcessor.plot_ui`:
       Plots the demand-to-capacity ratio, :math:`{\mathsf v_{ed}}/{
       \mathsf v_{rd}}`, at the most adverse control-perimeter, `Column.ui`.
       That is, at the perimeter with the largest demand-to-capacity ratio.
     * `SlabPostProcessor.create_dataframes`:
       Creates `pandas` dataframes indexed per column label for the
       following information:

       #. ``drop-panels``: Data about any drop-panels present.
       #. ``tensile-reinforcement``: Data about tensile reinforcement.
       #. ``design-checks``: The results at each control perimeter.
       #. ``shear-reinforcement``: Data about shear reinforcement.

The design output
-----------------

Column
``````

When calling for results on the slab, the design checks
are performed iteratively for each column.


The design output, in case of a column requiring shear reinforcement
has the following form::

   {'Position': 'internal',
    'Geometry': {
      'Centroid': {'x [m]': 65.35, 'y [m]': 49.858},
      'Bounding box': {'bx [mm]': 1000.0, 'by [mm]': 1500.0}},
    'Effective depth [mm]': 852.9,
    'Tensile Reinforcement': {
      'Effective tensile region': {
         'bx [mm]': 6117.6, 'by [mm]': 6117.6
         },
      'x-axis': 'T16@120 + T40@100',
      'y-axis': 'T16@120 + T40@100',
      'ρx': 0.0167,
      'ρy': 0.0167,
      'ρl': 0.0167},
    'Design': {
      'Perimeter u0': {
         'Most adverse loadcase': {
            'name': 'ULS/8',
            'Ved [kN]': 12130.1,
            'Mex [kN.m]': 638.7,
            'Mey [kN.m]': -1789.5,
            'ex [mm]': -147.5,
            'ey [mm]': -52.7},
         'β': 1.196,
         'ved [kPa]': 3400.6,
         'vrd [kPa]': 7380.0,
         'DCR (ved/vrd)': '0.461'},
     'Perimeter u1': {
         'Most adverse loadcase': {
            'name': 'ULS_Wind XY(+)/34',
            'Ved [kN]': 12126.3,
            'Mex [kN.m]': 32.5,
            'Mey [kN.m]': -1045.1,
            'ex [mm]': -86.2,
            'ey [mm]': -2.7},
         'β': 1.028,
         'ved [kPa]': 930.0,
         'vrd [kPa]': 751.6,
         'DCR (ved/vrd)': '1.237',
      'Shear Reinforcement': {
         'Basic': {
            'Asw/sr [mm2@100mm]': 882.8,
            'uout [mm]': 19448.1,
            'uouteff [mm]': 19458.6,
            'rout [mm]': 1216.8,
            'csx [mm]': 990.0,
            'csy [mm]': 1485.0},
         'Diagonal': {
            'nmin': '2',
            'rmin0 [mm]': 194.8,
            'rmax0 [mm]': 717.7,
            'cs [mm]': 100.0,
            'st [mm]': 100.0}}}}}

Note the following:

* ``Tensile reinforcement``: The ``Effective tensile region`` fields
  contains the geometric properties of the area of the effective tensile
  reinforcement, with reference to the centroid of the column. When
  the centroid of this regions does not coincide with the centroid of the
  column a relative offset is printed as follows::

      'Tensile Reinforcement': {
         'Effective tensile region': {
            'bx [mm]': 4800.8, 'by [mm]': 4800.8,
            'centroid': {
               'dx [mm]': '558.4', 'dy [mm]': '-0.0'}
               }}

* ``Perimeter u#``: This is the output for each perimeter calculated
  in accordance to the provisions of §6.4.2. When drop-panels are present
  and the conditions of §6.4.2(9) are met, the user should thus expect an
  additional field ``Perimeter u2`` with similar results.

* ``Shear reinforcement``: A configuration of shear cages that comply with
  §6.4.5. The layout is distinguished into basic (see Fig. a) and diagonal
  (Fig. b).

  .. figure:: /_static/shear_reinforcement_basic.png
     :alt: Basic shear reinforcement
     :width: 60%
     :align: center

     **\(a\)** Basic shear reinforcement

     :math:`d`: The effective depth of the slab.

  .. figure:: /_static/shear_reinforcement_diagonal.png
     :alt: Diagonal shear reinforcement
     :width: 60%
     :align: center

     **\(b\)** Diagonal shear reinforcement


Examples
--------

.. toctree::
   :hidden:
   :glob:

   examples/ex*

0. :doc:`Workflow using the API <examples/ex0_api_usage>`
1. :doc:`Basic control perimeters <examples/ex1_verify_control_perimeter>`
2. :doc:`Design check of single angle column <examples/ex2_single_column_angle>`
3. :doc:`Design of slab with chamfered corner and aligned column
   <examples/ex3_design_example_chamfer_slab_aligned_column>`

*Interactive jupyter notebooks of these and additional examples can be found
in the* `source code`_ (see ``notebooks/``).


.. _source code : https://gitlab.com/d-e/dx-punch/
