Example: Design of slab with chamfered corner and aligned column
================================================================

.. code:: python

    """A design example of a fully configured slab-column
    configuration.
    
    The calculation is consistent with EC2 §6.4. Implementation
    of §6.4.5 is still pending
    """
    import matplotlib.pyplot as plt
    
    from math import pi, sin, cos
    from dx_punch.EC2.slab import Slab
    from dx_eurocode.EC2.materials import RC
    %matplotlib


.. parsed-literal::

    Using matplotlib backend: TkAgg


.. code:: python

    # A tentative input data-structure
    input_data = {
        'slab': {
            'geometry': {
                'shape': 'generic',
                'vertices': [
                    (0., 0.), (6.25, 0.), (7.25, 1.), (7.25, 11.45),
                    (0., 11.45),
                    ],
                'thickness': 0.30,
                },
            'materials': {
                'concrete': 'C30',
                'steel': None
                },
            'reinforcement': {
                'entire-slab': [{
                    'axes': 'xy',
                    'phi': 0.010,
                    's': 0.200, # spacing
                    'd': 0.27 # effective depth
                    },{
                    'axes': 'xy',
                    'phi': 0.010,
                    's': 0.200, # spacing
                    'd': 0.29 # effective depth
                    },
                    ],
                'partial': [{
                    'geometry': {
                        'shape': 'rectangle',
                        'bx': 1.5,
                        'by': 10.,
                        'position': 'lower-left'
                        },
                    'axes': 'x',
                    'phi': 0.010,
                    's': 0.200, # spacing
                    'd': 0.23 # effective depth
                    },
                    ],
                },
            'columns': [{
                'geometry': {
                    'shape': 'rectangle',
                    'bx': 0.25,
                    'by': 0.25,
                    'origin': (0.125, 11.325),
                    },
                'forces': {
                    'N': 111e+03,
                    'Mex': 25e+03,
                    'Mey': 22e+03,
                    }
                }, {
                'geometry': {
                    'shape': 'rectangle',
                    'bx': 0.25,
                    'by': 0.25,
                    'origin': (0.125, 5.725)
                    },
                'forces': {
                    'N': 266e+03,
                    'Mex': 42e+03,
                    'Mey': 0.,
                    }
                }, {
                'geometry': {
                    'shape': 'rectangle',
                    'bx': 0.25,
                    'by': 0.25,
                    'origin': (0.125, 0.125),
                    },
                'forces': {
                    'N': 112e+03,
                    'Mex': 25e+03,
                    'Mey': 22e+03,
                    }
                }, {
                'geometry': {
                    'shape': 'rectangle',
                    'bx': 0.25,
                    'by': 0.25,
                    'origin': (6.125, 11.325),
                    },
                'forces': {
                    'N': 252e+03,
                    'Mex': 3e+03,
                    'Mey': 36e+03,
                    }
                }, {
                'geometry': {
                    'shape': 'rectangle',
                    'bx': 0.25,
                    'by': 0.25,
                    'origin': (6.125, 5.725), 
                    },
                'forces': {
                    'N': 664e+03,
                    'Mex': 8e+03,
                    'Mey': 1e+03,
                    }
                }, {
                'geometry': {
                    'shape': 'generic',
                    'vertices': [
                        (5.75, 0.), (6.25, 0.), (6.25+0.5*cos(pi/4), 0.5*sin(pi/4)),
                        (6.25+0.25*cos(pi/4), 0.75*sin(pi/4)),
                        (6.25-0.25*sin(pi/8), 0.25*cos(pi/8)), (5.75, 0.25)
                        ]
                    },
                'forces': {
                    'N': 246e+03,
                    'Mex': 6e+03,
                    'Mey': 34e+03,
                    }
                }, 
                ]
            }
        }

.. code:: python

    # Configure the slab
    slab_data = input_data['slab']
    slab = Slab.new(**slab_data['geometry'], material=RC[int(slab_data['materials']['concrete'][1:])])
    # -> reinforcement distribution
    for rebar in slab_data['reinforcement']['entire-slab']:
        slab.add_uniform_rebar(phi=rebar['phi'], s=rebar['s'], d=rebar['d'], axes=rebar['axes'])
    for rebar in slab_data['reinforcement']['partial']:
        slab.add_partial_rebar(phi=rebar['phi'], s=rebar['s'], d=rebar['d'], axes=rebar['axes'], **rebar['geometry'])

.. code:: python

    # -> Visualize slab and reinforcement areas
    plt.plot(slab.boundary.xy[0], slab.boundary.xy[1])
    for i, rebar in enumerate(slab.rebar):
        plt.plot(rebar.geometry.boundary.xy[0], rebar.geometry.boundary.xy[1], 'g--')



.. image:: ex3_output_3_0.png


.. code:: python

    # -> Add columns
    for column in slab_data['columns']:
        slab.add_column(**column['geometry'], **column['forces'])

.. code:: python

    plt.plot(slab.boundary.xy[0], slab.boundary.xy[1])
    for column in slab.columns:
        plt.plot(column.boundary.xy[0], column.boundary.xy[1], 'k')
        u = column.ui
        plt.plot(u.boundary.xy[0], u.boundary.xy[1], 'r')
        plt.plot(column.tensile.effective_region.boundary.xy[0], column.tensile.effective_region.boundary.xy[1], 'y')



.. image:: ex3_output_5_0.png


.. code:: python

    for i, column in enumerate(slab.columns):
        print(f'==> Column {i+1}:')
        column.print()


.. parsed-literal::

    ==> Column 1:
      -> Column "C4144": 
           - Results:
             * Position: corner
             * Geometry:
               + Centroid:
                 - x [m]: 0.125
                 - y [m]: 11.325
               + Bounding box:
                 - bx [mm]: 250.0
                 - by [mm]: 250.0
             * Effective depth [mm]: 280.0
             * Tensile Reinforcement:
               + Effective tensile region:
                 - bx [mm]: 1090.0
                 - by [mm]: 1090.0
               + x-axis: T10@200 + T10@200
               + y-axis: T10@200 + T10@200
               + ρx: 0.0028
               + ρy: 0.0028
               + ρl: 0.0028
             * Design:
               + Perimeter u0:
                 - Most adverse loadcase:
                   * name: LC0
                   * Ved [kN]: 111.0
                   * Mex [kN.m]: 25.0
                   * Mey [kN.m]: 22.0
                   * ex [mm]: -198.2
                   * ey [mm]: 225.2
                 - β: 2.919
                 - ved [kPa]: 2314.4
                 - vrd [kPa]: 5280.0
                 - DCR (ved/vrd): 0.438
               + Perimeter u1:
                 - Most adverse loadcase:
                   * name: LC0
                   * Ved [kN]: 111.0
                   * Mex [kN.m]: 25.0
                   * Mey [kN.m]: 22.0
                   * ex [mm]: -198.2
                   * ey [mm]: 225.2
                 - β: 1.407
                 - ved [kPa]: 404.3
                 - vrd [kPa]: 480.5
                 - DCR (ved/vrd): 0.841
    ==> Column 2:
      -> Column "C5992": 
           - Results:
             * Position: edgex
             * Geometry:
               + Centroid:
                 - x [m]: 0.125
                 - y [m]: 5.725
               + Bounding box:
                 - bx [mm]: 250.0
                 - by [mm]: 250.0
             * Effective depth [mm]: 271.7
             * Tensile Reinforcement:
               + Effective tensile region:
                 - bx [mm]: 1065.0
                 - by [mm]: 1065.0
               + x-axis: T10@200 + T10@200 + 0
               + y-axis: T10@200 + T10@200 + 0
               + ρx: 0.0043
               + ρy: 0.0029
               + ρl: 0.0035
             * Design:
               + Perimeter u0:
                 - Most adverse loadcase:
                   * name: LC0
                   * Ved [kN]: 266.0
                   * Mex [kN.m]: 42.0
                   * Mey [kN.m]: 0.0
                   * ex [mm]: -0.0
                   * ey [mm]: 157.9
                 - β: 2.01
                 - ved [kPa]: 2624.1
                 - vrd [kPa]: 5280.0
                 - DCR (ved/vrd): 0.497
               + Perimeter u1:
                 - Most adverse loadcase:
                   * name: LC0
                   * Ved [kN]: 266.0
                   * Mex [kN.m]: 42.0
                   * Mey [kN.m]: 0.0
                   * ex [mm]: -0.0
                   * ey [mm]: 157.9
                 - β: 1.202
                 - ved [kPa]: 479.0
                 - vrd [kPa]: 490.1
                 - DCR (ved/vrd): 0.977
    ==> Column 3:
      -> Column "C6272": 
           - Results:
             * Position: corner
             * Geometry:
               + Centroid:
                 - x [m]: 0.125
                 - y [m]: 0.125
               + Bounding box:
                 - bx [mm]: 250.0
                 - by [mm]: 250.0
             * Effective depth [mm]: 271.7
             * Tensile Reinforcement:
               + Effective tensile region:
                 - bx [mm]: 1065.0
                 - by [mm]: 1065.0
               + x-axis: T10@200 + T10@200 + 0
               + y-axis: T10@200 + T10@200 + 0
               + ρx: 0.0043
               + ρy: 0.0029
               + ρl: 0.0035
             * Design:
               + Perimeter u0:
                 - Most adverse loadcase:
                   * name: LC0
                   * Ved [kN]: 112.0
                   * Mex [kN.m]: 25.0
                   * Mey [kN.m]: 22.0
                   * ex [mm]: -196.4
                   * ey [mm]: 223.2
                 - β: 2.902
                 - ved [kPa]: 2392.8
                 - vrd [kPa]: 5280.0
                 - DCR (ved/vrd): 0.453
               + Perimeter u1:
                 - Most adverse loadcase:
                   * name: LC0
                   * Ved [kN]: 112.0
                   * Mex [kN.m]: 25.0
                   * Mey [kN.m]: 22.0
                   * ex [mm]: -196.4
                   * ey [mm]: 223.2
                 - β: 1.226
                 - ved [kPa]: 373.6
                 - vrd [kPa]: 490.1
                 - DCR (ved/vrd): 0.762
    ==> Column 4:
      -> Column "C4648": 
           - Results:
             * Position: edgey
             * Geometry:
               + Centroid:
                 - x [m]: 6.125
                 - y [m]: 11.325
               + Bounding box:
                 - bx [mm]: 250.0
                 - by [mm]: 250.0
             * Effective depth [mm]: 280.0
             * Tensile Reinforcement:
               + Effective tensile region:
                 - bx [mm]: 1930.0
                 - by [mm]: 1930.0
               + x-axis: T10@200 + T10@200
               + y-axis: T10@200 + T10@200
               + ρx: 0.0028
               + ρy: 0.0028
               + ρl: 0.0028
             * Design:
               + Perimeter u0:
                 - Most adverse loadcase:
                   * name: LC0
                   * Ved [kN]: 252.0
                   * Mex [kN.m]: 3.0
                   * Mey [kN.m]: 36.0
                   * ex [mm]: -142.9
                   * ey [mm]: 11.9
                 - β: 1.917
                 - ved [kPa]: 2300.4
                 - vrd [kPa]: 5280.0
                 - DCR (ved/vrd): 0.436
               + Perimeter u1:
                 - Most adverse loadcase:
                   * name: LC0
                   * Ved [kN]: 252.0
                   * Mex [kN.m]: 3.0
                   * Mey [kN.m]: 36.0
                   * ex [mm]: -142.9
                   * ey [mm]: 11.9
                 - β: 1.181
                 - ved [kPa]: 423.7
                 - vrd [kPa]: 480.5
                 - DCR (ved/vrd): 0.882
    ==> Column 5:
      -> Column "C4536": 
           - Results:
             * Position: edgex
             * Geometry:
               + Centroid:
                 - x [m]: 6.125
                 - y [m]: 5.725
               + Bounding box:
                 - bx [mm]: 250.0
                 - by [mm]: 250.0
             * Effective depth [mm]: 280.0
             * Tensile Reinforcement:
               + Effective tensile region:
                 - bx [mm]: 1930.0
                 - by [mm]: 1930.0
               + x-axis: T10@200 + T10@200
               + y-axis: T10@200 + T10@200
               + ρx: 0.0028
               + ρy: 0.0028
               + ρl: 0.0028
             * Design:
               + Perimeter u0:
                 - Most adverse loadcase:
                   * name: LC0
                   * Ved [kN]: 664.0
                   * Mex [kN.m]: 8.0
                   * Mey [kN.m]: 1.0
                   * ex [mm]: -1.5
                   * ey [mm]: 12.0
                 - β: 1.078
                 - ved [kPa]: 3407.5
                 - vrd [kPa]: 5280.0
                 - DCR (ved/vrd): 0.645
               + Perimeter u1:
                 - Most adverse loadcase:
                   * name: LC0
                   * Ved [kN]: 664.0
                   * Mex [kN.m]: 8.0
                   * Mey [kN.m]: 1.0
                   * ex [mm]: -1.5
                   * ey [mm]: 12.0
                 - β: 2.005
                 - ved [kPa]: 1054.7
                 - vrd [kPa]: 480.5
                 - DCR (ved/vrd): 2.195
                 - Shear Reinforcement:
                   * Basic:
                     + Asw/sr [mm2@100mm]: 480.1
                     + uout [mm]: 9897.8
                     + uouteff [mm]: 9911.9
                     + rout [mm]: 1835.4
                     + csx [mm]: 182.3
                     + csy [mm]: 182.3
                   * Diagonal:
                     + Minimum number: 5
                     + rmin0 [mm]: 231.5
                     + rmax0 [mm]: 284.7
                     + cs [mm]: 40.0
                     + st [mm]: 40.0
    ==> Column 6:
      -> Column "C3584": 
           - Results:
             * Position: corner
             * Geometry:
               + Centroid:
                 - x [m]: 6.169
                 - y [m]: 0.197
               + Bounding box:
                 - bx [mm]: 853.6
                 - by [mm]: 530.3
             * Effective depth [mm]: 280.0
             * Tensile Reinforcement:
               + Effective tensile region:
                 - bx [mm]: 2348.2
                 - by [mm]: 2348.2
               + x-axis: T10@200 + T10@200
               + y-axis: T10@200 + T10@200
               + ρx: 0.0028
               + ρy: 0.0028
               + ρl: 0.0028
             * Design:
               + Perimeter u0:
                 - Most adverse loadcase:
                   * name: LC0
                   * Ved [kN]: 246.0
                   * Mex [kN.m]: 6.0
                   * Mey [kN.m]: 34.0
                   * ex [mm]: -138.2
                   * ey [mm]: 24.4
                 - β: 1.394
                 - ved [kPa]: 1458.2
                 - vrd [kPa]: 5280.0
                 - DCR (ved/vrd): 0.276
               + Perimeter u1:
                 - Most adverse loadcase:
                   * name: LC0
                   * Ved [kN]: 246.0
                   * Mex [kN.m]: 6.0
                   * Mey [kN.m]: 34.0
                   * ex [mm]: -138.2
                   * ey [mm]: 24.4
                 - β: 1.182
                 - ved [kPa]: 407.0
                 - vrd [kPa]: 480.5
                 - DCR (ved/vrd): 0.847


.. code:: python

    slab.postprocessor.plot_ui(show_legend=False)




.. parsed-literal::

    (<Figure size 432x288 with 1 Axes>,
     <matplotlib.axes._subplots.AxesSubplot at 0x7fb49d6a3588>)




.. image:: ex3_output_7_1.png

