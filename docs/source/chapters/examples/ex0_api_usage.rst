Example: Demonstrate workflow using the API
-------------------------------------------

.. code:: python

   In [1]: from dx_punch.EC2 import Slab
   In [2]: from dx_eurocode.EC2.materials import RC

   # Configure slab
   In [3]: raft = Slab.new(
       shape='rectangle', bx=10., by=12., position='lower-left',
       thickness=300e-03, material=RC['20'], fyk=500.,
       slab_type='raft'
       )

   # Add column[s] with multiple loadcases
   In [4]: raft.add_column(
       bx=0.3, by=1., origin=(5., 5.), _id='C01', LC='ULS',
       N=365000, Mex=-15000, Mey=8000
       )
   In [5]: raft.add_column(_id='C01', LC='SLS', N=565000)

   # Inspect slab column
   In [6]: raft.columns
   Out[6]: [<dx_punch.EC2.column.Column at 0x7f296a05f7f0>]

   In [7]: raft.index
   Out[7]: {'C01': <dx_punch.EC2.column.Column at 0x7f296a05f7f0>}

   # Inspect column load-cases
   In [8]: column = raft.index['C01']

   In [9]: column.lc_index
   Out[9]:
   {'ULS': <dx_punch.EC2.forces.InternalForces at 0x7f296a05f860>,
    'SLS': <dx_punch.EC2.forces.InternalForces at 0x7f29690cfb38>}

   In [10]: column.forces
   Out[10]:
   array([[365000., -15000.,   8000.],
          [565000.,      0.,      0.]])

   # Add tensile reinforcement on the slab
   In [11]: raft.add_uniform_rebar(phi=16e-03, s=120e-03, axes='xy', d=275e-03)

   In [12]: column.deff
   Out[12]: 0.275

   # Define a drop panel for the column
   In [13]: column.set_drop_panel(lx=1.5, height=0.2)

   In [14]: column.drop_panel
   Out[14]: <dx_punch.EC2.column.DropPanel at 0x7f296a0cbbe0>

   In [15]: raft.add_soil_pressure(2000)

   # Add additional reinforcement in the vicinity of the column
   In [16]: partial_rebar_region = column.create_by_offset(source=column,
   dx=3*column.deff)

   In [17]: raft.add_partial_rebar(phi=20e-03, s=100e-03, d=250e-03, axes='xy',
   shape='generic', vertices=partial_rebar_region.boundary)

   In [18]: column.deff
   Out[18]: 0.25869565217391305

   # Ask for the design output
   In [19]: column.output

   Out[19]:
   {'Position': 'corner',
    'Geometry': {'Centroid': {'x [m]': 5.0, 'y [m]': 5.0},
     'Bounding box': {'bx [mm]': 300.0, 'by [mm]': 1000.0}},
    'Effective depth [mm]': 258.7,
    'Tensile Reinforcement': {'Effective tensile region': {'bx [mm]': 1852.2,
      'by [mm]': 1852.2,
      'Centroid (offset)': {'dx [mm]': '-0.0', 'dy [mm]': '-0.0'}},
     'x-axis': 'T16@120 + T20@100',
     'y-axis': 'T16@120 + T20@100',
     'ρx': 0.0186,
     'ρy': 0.0186,
     'ρl': 0.0186},
    'Drop panel': {'lx [mm]': 1500.0, 'ly [mm]': 1500.0, 'H [mm]': 200.0},
    'Design': {'Perimeter u0': {'Most adverse loadcase': {'name': 'SLS',
       'Ved [kN]': 565.0,
       'Mex [kN.m]': 0.0,
       'Mey [kN.m]': 0.0,
       'ex [mm]': 0.0,
       'ey [mm]': -0.0},
      'β': 1.0,
      'ved [kPa]': 1587.1,
      'vrd [kPa]': 3680.0,
      'DCR (ved/vrd)': '0.431'},
     'Perimeter u1': {'Most adverse loadcase': {'name': 'SLS',
       'Ved [kN]': 565.0,
       'Mex [kN.m]': 0.0,
       'Mey [kN.m]': 0.0,
       'ex [mm]': 0.0,
       'ey [mm]': -0.0},
      'β': 1.0,
      'ved [kPa]': 147.3,
      'vrd [kPa]': 753.1,
      'DCR (ved/vrd)': '0.196'},
     'Perimeter u2': {'Most adverse loadcase': {'name': 'ULS',
       'Ved [kN]': 365.0,
       'Mex [kN.m]': -15.0,
       'Mey [kN.m]': 8.0,
       'ex [mm]': 21.9,
       'ey [mm]': 41.1},
      'β': 3.728,
      'ved [kPa]': 380.6,
      'vrd [kPa]': 753.1,
      'DCR (ved/vrd)': '0.505'}}}
