.. Add links to use in source pages in the current folder

.. _shapely: https://shapely.readthedocs.io/en/stable/index.html#
.. _matplotlib: https://matplotlib.org/index.html
.. _dx-utilities: https://d-e.gitlab.io/dx-utilities/
.. _dx-base: https://d-e.gitlab.io/dx-base/
.. _dx-eurocode: https://d-e.gitlab.io/dx-eurocode/
