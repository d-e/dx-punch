# `dx-punch`

A punching shear calculation library as per Eurocode 2 (EN1992-1-1-2005)

# Installation

## System dependencies

* `python3 (>=3.6)`
* `python3-dev`
* `tkinter` (or other `matplotlib` backend)
* `make`

### Ubuntu

On ubuntu this takes care of everything (probably)

    sudo apt install python3-dev make build-essential

## Development version

This repository includes nested submodules. Thus it has to be cloned with the
appropriate options for initializing and cloning the submodules. For more
information on working with submodules see the relevant discussion
[here][nested-submodules] and the reference on [Pro Git][pro-git-submodules].

* Clone the repository

        $ git clone --recurse-submodules git@gitlab.com:d-e/dx-punch.git [local-path]

* Create virtual environment:

        $ make install

* Activate virtual environment and run tests:

        $ source venv/bin/activate
        $ nose2

### Run the jupyter notebooks

1. Start the jupyter notebook server

        (venv) $ source start-jupyter-server

2. Open the browser at `localhost:8888` and open a notebook (`*.ipynb`).


## As a dependency

### From source code

```
$ pip install git://git@gitlab.com/d-e/dx-punch.git#egg=dx_punch
```

### From Python Package Index (PyPI)

```
$ pip install dx-punch
```

## Features

### Design according to Eurocode provisions (EN-1992-1)

1. Evaluation of basic control perimeters (§6.4.2)

   * Support for columns of arbitrary polygonal shape.
   * Support for drop panels.
   * Automatic classification of columns into 'internal', 'edge',
     and 'corner'.

2. Evaluation of design shear stress (§6.4.3) for biaxial eccentricity
   of all classes of columns, using three methods:

   * Analytical via numerical evaluation of the perimeter modulus W1
     (6.40) in both directions.
   * Approximate through Eq. (6.43).
   * Simplified through §6.4.3(6).

3. Design checks according to §6.4.4.

   * Support for declaration of soil-pressure on raft-slabs:

     * Uniform
     * Discrete pressure field

4. Evaluation of a code-compliant shear-cage layout according to
   provisions of §6.4.5.

*Not yet supported*:

* Consideration of openings in the evaluation of basic control perimeters
  (§6.4.2(3)).
* Favourable effect of in-plane normal stresses in Eq. (6.47).
* Evaluation of radial layout of shear reinforcement (Fig. 6.22A).

### API

We use the `dx` stack of packages (`dx-utilities`, `dx-base`, `dx-eurocode`)
that enables:

1. Geometric representation of structural elements using `shapely`.
2. Geometric operations and linear algebra utilities with `numpy` and `mathutils`.
3. Use of `dx_utilities.fields` for various representations.
4. Visualization of results using `matplotlib`.
5. Support for tabular reports using `pandas`.

### Sample usage

```python
In [1]: from dx_punch.EC2 import Slab
In [2]: from dx_eurocode.EC2.materials import RC

# Configure slab
In [3]: raft = Slab.new(
    shape='rectangle', bx=10., by=12., position='lower-left',
    thickness=300e-03, material=RC['20'], fyk=500.,
    slab_type='raft'
    )

# Add column[s] with multiple loadcases
In [4]: raft.add_column(
    bx=0.3, by=1., origin=(5., 5.), _id='C01', LC='ULS',
    N=365000, Mex=-15000, Mey=8000
    )
In [5]: raft.add_column(_id='C01', LC='SLS', N=565000)

# Inspect slab column
In [6]: raft.columns
Out[6]: [<dx_punch.EC2.column.Column at 0x7f296a05f7f0>]

In [7]: raft.index
Out[7]: {'C01': <dx_punch.EC2.column.Column at 0x7f296a05f7f0>}

# Inspect column load-cases
In [8]: column = raft.index['C01']

In [9]: column.lc_index
Out[9]:
{'ULS': <dx_punch.EC2.forces.InternalForces at 0x7f296a05f860>,
 'SLS': <dx_punch.EC2.forces.InternalForces at 0x7f29690cfb38>}

In [10]: column.forces
Out[10]:
array([[365000., -15000.,   8000.],
       [565000.,      0.,      0.]])

# Add tensile reinforcement on the slab
In [11]: raft.add_uniform_rebar(phi=16e-03, s=120e-03, axes='xy', d=275e-03)

In [12]: column.deff
Out[12]: 0.275

# Define a drop panel for the column
In [13]: column.set_drop_panel(lx=1.5, height=0.2)

In [14]: column.drop_panel
Out[14]: <dx_punch.EC2.column.DropPanel at 0x7f296a0cbbe0>

In [15]: raft.add_soil_pressure(2000)

# Add additional reinforcement in the vicinity of the column
In [16]: partial_rebar_region = column.create_by_offset(source=column,
dx=3*column.deff)

In [17]: raft.add_partial_rebar(phi=20e-03, s=100e-03, d=250e-03, axes='xy',
shape='generic', vertices=partial_rebar_region.boundary)

In [18]: column.deff
Out[18]: 0.25869565217391305

# Ask for the design output
In [19]: column.output

Out[19]:
{'Position': 'corner',
 'Geometry': {'Centroid': {'x [m]': 5.0, 'y [m]': 5.0},
  'Bounding box': {'bx [mm]': 300.0, 'by [mm]': 1000.0}},
 'Effective depth [mm]': 258.7,
 'Tensile Reinforcement': {'Effective tensile region': {'bx [mm]': 1852.2,
   'by [mm]': 1852.2,
   'Centroid (offset)': {'dx [mm]': '-0.0', 'dy [mm]': '-0.0'}},
  'x-axis': 'T16@120 + T20@100',
  'y-axis': 'T16@120 + T20@100',
  'ρx': 0.0186,
  'ρy': 0.0186,
  'ρl': 0.0186},
 'Drop panel': {'lx [mm]': 1500.0, 'ly [mm]': 1500.0, 'H [mm]': 200.0},
 'Design': {'Perimeter u0': {'Most adverse loadcase': {'name': 'SLS',
    'Ved [kN]': 565.0,
    'Mex [kN.m]': 0.0,
    'Mey [kN.m]': 0.0,
    'ex [mm]': 0.0,
    'ey [mm]': -0.0},
   'β': 1.0,
   'ved [kPa]': 1587.1,
   'vrd [kPa]': 3680.0,
   'DCR (ved/vrd)': '0.431'},
  'Perimeter u1': {'Most adverse loadcase': {'name': 'SLS',
    'Ved [kN]': 565.0,
    'Mex [kN.m]': 0.0,
    'Mey [kN.m]': 0.0,
    'ex [mm]': 0.0,
    'ey [mm]': -0.0},
   'β': 1.0,
   'ved [kPa]': 147.3,
   'vrd [kPa]': 753.1,
   'DCR (ved/vrd)': '0.196'},
  'Perimeter u2': {'Most adverse loadcase': {'name': 'ULS',
    'Ved [kN]': 365.0,
    'Mex [kN.m]': -15.0,
    'Mey [kN.m]': 8.0,
    'ex [mm]': 21.9,
    'ey [mm]': 41.1},
   'β': 3.728,
   'ved [kPa]': 380.6,
   'vrd [kPa]': 753.1,
   'DCR (ved/vrd)': '0.505'}}}
```

## Contribute

Source code lives in https://gitlab.com/d-e/dx-punch.

### Code of conduct

We abide by the provisions of [Contributor Coventant Code of Conduct][COC].

### Workflow

Follow this [simplified gitflow model][gitflow].

### Coding standards

* Follow [PEP8 rules](https://www.python.org/dev/peps/pep-0008/).
* Populate docstrings with concise descriptions and the signature
  of the object according to [sphinx guidelines][sphinx-sig]. For a
  complete overview of documentation options see
  [Sphinx docs](http://www.sphinx-doc.org)
* Write unit-tests.

### Versioning

We follow [semantic versioning][semver].

### Review the docs locally

Documentation is generated through continuous-integration (CI). For
review purposes it can be generated locally with:

```
$ make MODPATH=../dx_punch -C docs apidoc html
```

For more details on the process run

```
$ make docs-help
```

### Error codes

The package includes coded exceptions. The list of error codes, along
with associated metadata is generated in [`ERROR_CODES.md`](ERROR_CODES.md).

Upon refactoring or additions of coded exceptions this list must be
regenerated through

```
$ make parse-error-codes
```

## Public API

See the [documentation pages](https://d-e.gitlab.io/dx-punch/).

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the [GNU Affero General Public License](LICENSE) as
published by the Free Software Foundation, either version 3 of the
License, or any later version.

[gitflow]: https://gitlab.com/d-e/dx-utilities/wikis/git-workflow
[semver]: https://semver.org/
[COC]: https://www.contributor-covenant.org/version/1/4/code-of-conduct
[sphinx-sig]: http://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#info-field-lists
[nested-submodules]: http://social.d-e.gr/techblog/posts/10-python-projects-with-git-submodules
[pro-git-submodules]: https://git-scm.com/book/en/v2/Git-Tools-Submodules
