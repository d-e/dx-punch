env_dir:=venv
pip:=$(env_dir)/bin/pip

docs-help:
	cat docs/README.rst
	make -C docs help

install:
	python3 -m venv $(env_dir)
	$(pip) install -r requirements.txt
	$(pip) install ipython
	$(pip) install --upgrade pip

clean:
	rm -r $(env_dir)

reinstall:
	make clean install

parse-error-codes:
	git grep -WE --recurse-submodules "raise \w{10,}\(" | \
	    python -m dx_utilities.bin.parse_error_codes > \
	    ERROR_CODES.md
